# cosc2640-assignment2

## Datasets

1. https://vicroadsopendata-vicroadsmaps.opendata.arcgis.com/datasets/traffic-count-locations
2. https://vicroadsopendata-vicroadsmaps.opendata.arcgis.com/datasets/traffic-count-locations
3. https://vicroadsopendata-vicroadsmaps.opendata.arcgis.com/datasets/147696bb47544a209e0a5e79e165d1b0_0
4. https://vicroadsopendata-vicroadsmaps.opendata.arcgis.com/datasets/traffic-lights/data

## VicRoads Open Data API

https://vicroadsopendata-vicroadsmaps.opendata.arcgis.com/datasets/traffic-lights/data

## Google Maps Directions Service Docs

https://developers.google.com/maps/documentation/javascript/directions
