/*******************************************************************************
 * vra-redshift-api/handler.js
 * 
 * Author:        Stephen Radley
 * Creation Date: 2019/01/27
 * 
 * ...
 ******************************************************************************/

'use strict';

'use strict';

const { Pool } = require('pg');

const pool = new Pool();

/**
 * @function getTrafficVolumeGraph ...
 * endpoint: api/graphs/traffic_volume
 */
module.exports.getTrafficVolumeGraph = (event, context, callback) => {
    const query = `SELECT f0_ as ROAD_NAME, SUM(allvehs_aadt) as TOTAL_VOLUME
FROM(
SELECT SPLIT_PART(HMGNS_LNK_DESC, 'btwn', 1) as f0_, ALLVEHS_AADT
FROM public.traffic_volume
)
GROUP BY ROAD_NAME
ORDER BY TOTAL_VOLUME DESC
LIMIT 10`;
    
  pool.query(query)
    .then(res => {
      console.log(res);
      var resp = {
        statusCode: 200,
        body: JSON.stringify(res.rows),
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true
        }
      };
      callback(null, resp);  // success
    })
    .catch(err => {
      console.error(err);
      callback(err, null);  // failure
    });
};

/** 
 * @function getDangerousRoadsGraph ...
 * endpoint: api/graphs/dangerous_roads
 */
module.exports.getDangerousRoadsGraph = (event, context, callback) => {
  const query = `SELECT ROAD_NAME, SUM(SEVERITY) AS RISK
FROM (SELECT ROAD_NAME, CAST(SEVERITY AS NUMERIC) as SEVERITY FROM public.crashes_last_five_years_w_roadnames)
GROUP BY ROAD_NAME, SEVERITY 
ORDER BY RISK DESC
LIMIT 10;`;
    
  pool.query(query)
    .then(res => {
      console.log(res);
      var resp = {
        statusCode: 200,
        body: JSON.stringify(res.rows),
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true
        }
      };
      callback(null, resp);  // success
    })
    .catch(err => {
      console.error(err);
      callback(err, null);  // failure
    });
};

/**
 * @function getMonthlyCrashesGraph ...
 * endpoint: api/graphs/monthly_crashes
 */
module.exports.getMonthlyCrashesGraph = (event, context, callback) => {
  const query = `SELECT EXTRACT(YEAR FROM to_timestamp(ACCIDENT_DATE, 'DD/MM/YYYY')) as YEAR, EXTRACT(MONTH FROM to_timestamp(ACCIDENT_DATE, 'DD/MM/YYYY')) as MONTH, SUM(FATALITY) as FATALITY, SUM(SERIOUSINJURY) as SERIOUSINJURY, 
SUM(OTHERINJURY) as OTHERINJURY, SUM(NONINJURED) as NONINJURED, SUM(FATALITY+SERIOUSINJURY+OTHERINJURY+NONINJURED) as TOTAL
FROM public.crashes_last_five_years
GROUP BY YEAR, MONTH
ORDER BY YEAR ASC, MONTH ASC;`;

  pool.query(query)
    .then(res => {
      console.log(res);
      var resp = {
        statusCode: 200,
        body: JSON.stringify(res.rows),
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true
        }
      };
      callback(null, resp);  // success
    })
    .catch(err => {
      console.error(err);
      callback(err, null);  // failure
    });
};