# VicRoads Analytics Serverless Stack

- VRA Payload - contains the client payload, sets up cloudfront, and S3 Bucket for static site hosting.
- VRA DynamoDB API - the api layer which is dynamoDB dependant.
- VRA Redshift API - the api layer which is redshift dependant.
- VRA Cron - cron lambdas which keeps the VicRoads data updated.
