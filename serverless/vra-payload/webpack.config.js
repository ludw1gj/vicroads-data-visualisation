const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const projectDir = path.resolve(__dirname);
const publicDir = `${projectDir}/public`;
const buildDir = `${projectDir}/build`;
const buildAssetsDir = `${buildDir}/assets`;
const srcDir = `${projectDir}/src`;
const srcAssetsDir = `${srcDir}/assets`;

const devPlugins = [
  new HtmlWebpackPlugin({
    template: `${publicDir}/index.html`,
    filename: './index.html',
  }),
];

const prodPlugins = [
  new CleanWebpackPlugin(['build']),
  new HtmlWebpackPlugin({
    template: `${publicDir}/index.html`,
    filename: './index.html',
  }),
  new CopyWebpackPlugin([
    {
      from: publicDir,
      to: buildDir,
    },
    {
      from: publicDir,
      to: buildDir,
    },
    {
      from: srcAssetsDir,
      to: buildAssetsDir,
    },
  ]),
];

const config = {
  entry: ['@babel/polyfill', `${srcDir}/index.js`],

  output: {
    filename: 'bundle.js',
    path: `${buildDir}`,
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      },
    ],
  },

  resolve: {
    extensions: ['.js', '.jsx'],
  },

  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    compress: true,
    port: 8080,
    historyApiFallback: true,
  },

  node: { fs: 'empty' },
};

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.devtool = 'source-map';
    config.plugins = [...devPlugins];
  }

  if (argv.mode === 'production') {
    config.plugins = [...prodPlugins];
  }
  return config;
};
