import HomeIcon from '@material-ui/icons/Home';
import MapIcon from '@material-ui/icons/Map';
import BarChartIcon from '@material-ui/icons/BarChart';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';

export const routes = [
  { name: 'Home', url: '/', icon: HomeIcon },
  { name: 'Graphs', url: '/graphs', icon: BarChartIcon },
  { name: 'Directions', url: '/map-mapbox', icon: DirectionsCarIcon },
  { name: 'Map', url: '/map-kepler', icon: MapIcon },
];

export const KEPLER_MAP_ID = 'keplerGlMap';
export const API_URL = 'https://z8az0gxtxe.execute-api.ap-southeast-2.amazonaws.com/dev';
export const MAPS_PAST_DATA_API_URL =
  'https://z8az0gxtxe.execute-api.ap-southeast-2.amazonaws.com/dev/api/maps/pastData/';
export const MAPS_REALTIME_DATA_API_URL =
  'https://z8az0gxtxe.execute-api.ap-southeast-2.amazonaws.com/dev/api/maps/realTime/';
export const GRAPHS_DATA_API_URL = 'https://uxzhocm965.execute-api.ap-southeast-2.amazonaws.com/dev/api/graphs/';

export const MAPBOX_ACCESS_TOKEN =
  'pk.eyJ1Ijoic3JhZGxleSIsImEiOiJjanF6aWd0NGUwZXRnNDNwbjJtZWQzdG51In0.aITvzHxIw2u7dknFbqg-5w';
