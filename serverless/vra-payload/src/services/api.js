import axios from 'axios';
import Processors from 'kepler.gl/processors';

import { GRAPHS_DATA_API_URL } from './config';

/** Get and serialise geojson data for a kepler.gl dataset.
 * @param {string} url - the url for geojson payload.
 * @param {string} id - the id for the dataset.
 */
export const queryGeoJSONDataset = async (url, id) => {
  try {
    const res = await axios.get(url);
    const data = Processors.processGeojson(res.data);
    const dataset = {
      data,
      info: {
        id: id,
      },
    };
    return dataset;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const queryDangerousRoadsData = () => axios.get(`${GRAPHS_DATA_API_URL}dangerous_roads`);
export const queryBusiestRoadsData = () => axios.get(`${GRAPHS_DATA_API_URL}traffic_volume`);
export const queryMonthlyCrashesData = () => axios.get(`${GRAPHS_DATA_API_URL}monthly_crashes`);
