import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import AppRouter from './router';
import AppNavbar from './components/AppNavbar';
import AppContent from './components/generic/AppContent';

/** The main component. */
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className='App'>
          <AppNavbar />
          <AppContent>
            <AppRouter />
          </AppContent>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
