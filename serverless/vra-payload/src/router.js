import React from 'react';
import { Switch, Route } from 'react-router-dom';

import NotFoundView from './views/NotFoundView';
import IndexView from './views/IndexView';
import GraphsView from './views/GraphsView';
import KeplerMapView from './views/KeplerMapView';
import MapboxMapView from './views/MapboxMapView';

/** AppRouter component handles routing. */
const AppRouter = () => {
  return (
    <Switch>
      <Route exact path='/' component={IndexView} />
      <Route exact path='/map-kepler' component={KeplerMapView} />
      <Route exact path='/map-mapbox' component={MapboxMapView} />
      <Route exact path='/graphs' component={GraphsView} />
      <Route path='*' component={NotFoundView} />
    </Switch>
  );
};

export default AppRouter;
