import React from 'react';

import DocumentTitle from '../components/DocumentTitle';
import Onboarding from '../components/generic/Onboarding';

const IndexView = () => {
  return (
    <React.Fragment>
      <DocumentTitle>Home | VicRoads Analytics</DocumentTitle>
      <Onboarding />
    </React.Fragment>
  );
};

export default IndexView;
