import React from 'react';
import MapboxMap from '../components/MapboxMap';
import DocumentTitle from '../components/DocumentTitle';

const DirectionsView = () => {
  return (
    <React.Fragment>
      <DocumentTitle>Direction | VicRoads Analytics</DocumentTitle>
      <MapboxMap />
    </React.Fragment>
  );
};

export default DirectionsView;
