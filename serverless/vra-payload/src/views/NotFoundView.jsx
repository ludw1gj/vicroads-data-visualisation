import React from 'react';

import DocumentTitle from '../components/DocumentTitle';

const NotFoundView = () => {
  return (
    <React.Fragment>
      <DocumentTitle>Not Found | VicRoads Analytics</DocumentTitle>
      <p>Page Not Found...</p>
    </React.Fragment>
  );
};

export default NotFoundView;
