import React from 'react';
import styled from 'styled-components';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import { queryDangerousRoadsData, queryBusiestRoadsData, queryMonthlyCrashesData } from '../services/api';
import LoadingCircle from '../components/generic/LoadingCircle';
import DocumentTitle from '../components/DocumentTitle';

class DangerousRoadsGraph extends React.Component {
  state = {
    data: null,
  };

  componentDidMount() {
    queryDangerousRoadsData().then(resp => {
      this.setState({ data: resp.data });
    });
  }

  render() {
    if (!this.state.data) {
      return <LoadingCircle />;
    }
    return (
      <BarChart width={800} height={300} data={this.state.data} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
        <CartesianGrid strokeDasharray='3 3' />
        <YAxis width={125} label='Risk' />
        <XAxis hide={true} dataKey='road_name' label='Roads' />

        <Tooltip />
        <Legend />
        <Bar dataKey='risk' fill='#8884d8' />
      </BarChart>
    );
  }
}

class BusiestRoadsGraph extends React.Component {
  state = {
    data: null,
  };

  componentDidMount() {
    queryBusiestRoadsData().then(resp => {
      this.setState({ data: resp.data });
    });
  }

  render() {
    if (!this.state.data) {
      return <LoadingCircle />;
    }
    return (
      <BarChart width={800} height={300} data={this.state.data} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
        <CartesianGrid strokeDasharray='3 3' />
        <YAxis width={260} label='Total Volume' />
        <XAxis hide={true} dataKey='road_name' label='Roads' />

        <Tooltip />
        <Legend />
        <Bar dataKey='total_volume' fill='#8884d8' />
      </BarChart>
    );
  }
}

class MonthlyCrashesGraph extends React.Component {
  state = {
    data: null,
  };

  componentDidMount() {
    queryMonthlyCrashesData().then(resp => {
      const data = resp.data.map(val => {
        val['label'] = `${val.year}.${val.month}`;
        return val;
      });
      this.setState({ data });
    });
  }

  render() {
    if (!this.state.data) {
      return <LoadingCircle />;
    }
    return (
      <BarChart width={1200} height={300} data={this.state.data} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
        <CartesianGrid strokeDasharray='3 3' />
        <YAxis width={200} label='Crash Severity' />
        <XAxis dataKey='label' />

        <Tooltip />
        <Legend />
        <Bar stackId='a' dataKey='fatality' fill='#ff7300' />
        <Bar stackId='a' dataKey='seriousinjury' fill='#82ca9d' />
        <Bar stackId='a' barSize={50} dataKey='otherinjury' fill='#8884d8' />
      </BarChart>
    );
  }
}

const GraphsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  margin-bottom: 50px;
`;

const StyledCard = styled(Card)`
  height: 350px;
  max-width: 1250px;
  margin: 20px;
`;

const StyledCardContent = styled(CardContent)`
  display: flex;
  justify-content: center;
  align-content: center;
`;

const GraphParagraph = styled.p`
  margin-left: 15%;
  margin-right: 15%;
`;

const GraphsView = () => {
  return (
    <React.Fragment>
      <DocumentTitle>Graphs | VicRoads Analytics</DocumentTitle>
      <GraphsContainer>
        <h1>Data Graphs</h1>

        <h2>Top 10 Busiest Roads of 2017</h2>
        <StyledCard style={{ width: '800px' }}>
          <StyledCardContent>
            <BusiestRoadsGraph />
          </StyledCardContent>
        </StyledCard>
        <GraphParagraph>
          Here we can see what the top 10 busiest roads were of 2017. This has summed all the traffic collected from
          traffic survey’s and estimates for a particular stretch of road. When the 2018 data set is released a more up
          to date version of this graph can be shown.
        </GraphParagraph>

        <h2>Top 10 Most Dangerous Roads</h2>
        <StyledCard style={{ width: '800px' }}>
          <StyledCardContent>
            <DangerousRoadsGraph />
          </StyledCardContent>
        </StyledCard>
        <GraphParagraph>
          This graph displays the top 10 most dangerous roads of Victoria. Dangerous in this sense doesn’t just mean
          fatalities in includes all accidents involved on the road. In order to decided what constituted a dangerous
          road, each accident is classified under 4 headings (Fatality, serious injury, other injury and non-injured).
          Each heading is weighted differently, fatality’s weighing the most, and non-injured being the least weighted.
          Therefore, if a road has a few accidents but they are mostly fatal it would be more dangerous than a road that
          has more non-injured crashes. Also, as this data goes back to 2013 the older the accident is the less
          dangerous it is considered.
        </GraphParagraph>

        <h2>Accidents Throughout the Months</h2>
        <StyledCard style={{ width: '1200px' }}>
          <StyledCardContent>
            <MonthlyCrashesGraph />
          </StyledCardContent>
        </StyledCard>
        <GraphParagraph>
          This stacked bar graph shows the individual types of accidents throughout the months of the year. Here we hope
          to see if there are any trends throughout the year such as an increase/decrease in non-injury accidents over
          the year or fatalities or perhaps an increase/decrease of total accidents throughout the year.
        </GraphParagraph>
      </GraphsContainer>
    </React.Fragment>
  );
};

export default GraphsView;
