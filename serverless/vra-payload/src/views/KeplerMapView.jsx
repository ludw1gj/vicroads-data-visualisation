import React from 'react';

import KeplerMap from '../components/KeplerMap';
import DocumentTitle from '../components/DocumentTitle';
import { KEPLER_MAP_ID } from '../services/config';

const MapView = () => {
  return (
    <React.Fragment>
      <DocumentTitle>Map | VicRoads Analytics</DocumentTitle>
      <KeplerMap id={KEPLER_MAP_ID} />
    </React.Fragment>
  );
};

export default MapView;
