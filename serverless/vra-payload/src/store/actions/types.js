export const MAKE_READONLY = 'MAKE_READONLY';

export const GET_GEOJSON_DATASET = 'GET_GEOJSON_DATASET';
export const DEFAULT_STATE = 'DEFAULT_STATE';
