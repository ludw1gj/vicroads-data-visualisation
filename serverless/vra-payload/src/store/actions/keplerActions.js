import { MAKE_READONLY, DEFAULT_STATE } from './types';

export const makeReadonly = () => ({ type: MAKE_READONLY });
export const defaultState = () => ({ type: DEFAULT_STATE });
