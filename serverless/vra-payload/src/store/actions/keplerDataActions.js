import { GET_GEOJSON_DATASET } from './types';
import { queryGeoJSONDataset } from '../../services/api';

/** Get geojson data for a kepler.gl dataset.
 *
 * @param {string} url - the url for geojson payload.
 * @param {string} id - the id for the dataset.
 */
export const getGeoJSONDataset = (url, id) => {
  return (dispatch, _) => {
    queryGeoJSONDataset(url, id)
      .then(dataset => {
        dispatch({ type: GET_GEOJSON_DATASET, payload: dataset });
      })
      .catch(error => {
        console.log(error);
      });
  };
};
