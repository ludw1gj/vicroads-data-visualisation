import { GET_GEOJSON_DATASET } from '../actions/types';

const initState = {
  dataset: null,
};

/** Reducer containing categories data. */
export const keplerDataReducer = (state = initState, action) => {
  switch (action.type) {
    case GET_GEOJSON_DATASET:
      return {
        dataset: action.payload,
      };

    default:
      return state;
  }
};
