import { MAKE_READONLY, DEFAULT_STATE } from '../actions/types';

export const keplerReducer = (state, action) => {
  switch (action.type) {
    case MAKE_READONLY:
      return {
        ...state,
        keplerGl: {
          ...state.keplerGl,

          keplerGlMap: {
            ...state.keplerGl.keplerGlMap,
            uiState: {
              ...state.keplerGl.keplerGlMap.uiState,
              readOnly: true,
              currentModal: null,
              mapControls: {
                ...state.keplerGl.keplerGlMap.uiState.mapControls,
                splitMap: {
                  show: false,
                },
              },
            },
          },
        },
      };
    case DEFAULT_STATE:
      return {
        ...state,
        keplerGl: {
          ...state.keplerGl,

          keplerGlMap: {
            ...state.keplerGl.keplerGlMap,
            mapState: {
              ...state.keplerGl.keplerGlMap.mapState,
              bearing: 9.85,
              latitude: -37.8136,
              longitude: 144.9631,
              pitch: 50,
              zoom: 10.5,
              dragRotate: true,
              isSplit: false,
            },
          },
        },
      };
    default:
      return null;
  }
};
