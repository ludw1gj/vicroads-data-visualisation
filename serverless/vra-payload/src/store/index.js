import { createStore, combineReducers, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import keplerGlReducer from 'kepler.gl/reducers';
import { taskMiddleware } from 'react-palm/tasks';

import { keplerReducer } from './reducers/keplerReducer';
import { keplerDataReducer } from './reducers/KeplerDataReducer';

const reducers = combineReducers({
  keplerData: keplerDataReducer,
  keplerGl: keplerGlReducer,
});

const composedReducer = (state, action) => {
  const newState = keplerReducer(state, action);
  if (newState) {
    return newState;
  }
  return reducers(state, action);
};

const store = createStore(composedReducer, applyMiddleware(taskMiddleware, reduxThunk));

export default store;
