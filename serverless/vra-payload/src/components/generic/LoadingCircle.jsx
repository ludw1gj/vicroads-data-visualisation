import React from 'react';
import styled from 'styled-components';

import CircularProgress from '@material-ui/core/CircularProgress';

const CircularProgressStyled = styled(CircularProgress)`
  color: green !important;
  margin-top: 15px;
`;

const LoadingCircle = () => {
  return <CircularProgressStyled />;
};

export default LoadingCircle;
