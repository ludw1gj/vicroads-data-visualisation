import styled from 'styled-components';

/** AppContent component which the web app's main content sits in. Minus the Navbar and Footer.  */
const AppContent = styled.div`
  min-height: calc(100vh - 64px);
`;

export default AppContent;
