import React from 'react';
import styled from 'styled-components';

// @ts-ignore
import backgroundImg from '../../assets/images/road.jpg';

const OnboardingContainer = styled.div`
  margin: 0;
  height: calc(100vh - 64px);
  width: 100vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: absolute;

  background: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url(${backgroundImg}) no-repeat center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

  color: white;
  text-shadow: 0 3px 2px rgba(0, 0, 0, 0.4), 0 8px 12px rgba(0, 0, 0, 0.1), 0 18px 22px rgba(0, 0, 0, 0.1);
`;

const Heading = styled.h1`
  margin-top: 25vh;
  font-size: 3em;
`;

const SupportingText = styled.p`
  font-size: 1.15em;
  margin-top: 0;
`;

const Onboarding = () => {
  return (
    <OnboardingContainer>
      <Heading>VicRoads Analytics</Heading>
      <SupportingText>Get real-time traffic data, find the top 10 most dangerous roads, and more!</SupportingText>
    </OnboardingContainer>
  );
};

export default Onboarding;
