import React, { Component } from 'react';
import { AutoSizer } from 'react-virtualized';
import MapboxGl from 'mapbox-gl/dist/mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions';
import '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions.css';

import { MAPBOX_ACCESS_TOKEN, MAPS_REALTIME_DATA_API_URL } from '../services/config';
import mapStyle from '../assets/config/mapboxgl/mapStyle.json';
import mapLayers from '../assets/config/mapboxgl/mapLayers.json';

const styles = {
  dataControls: {
    position: 'absolute',
    zIndex: 1,
    top: '10px',
    right: '10px',
  },
};

class MapboxMap extends Component {
  state = {
    map: null,
    sources: [],
    currentLayer: '',
  };

  componentDidMount() {
    MapboxGl.accessToken = MAPBOX_ACCESS_TOKEN;

    // @ts-ignore
    const mapboxDirections = new MapboxDirections({
      accessToken: MAPBOX_ACCESS_TOKEN,
    });
    const _map = new MapboxGl.Map({
      container: 'map',
      style: mapStyle,
      center: [144.9631, -37.8136],
      zoom: 12.5,
      attributionControl: false,
    });
    _map.addControl(mapboxDirections, 'top-left');

    this.setState({ map: _map });
  }

  updateMap = event => {
    if (this.state.currentLayer !== '') {
      this.state.map.removeLayer(this.state.currentLayer);
    }

    if (!this.state.sources.includes(event.target.value)) {
      this.state.map.addSource(event.target.value, {
        type: 'geojson',
        data: MAPS_REALTIME_DATA_API_URL + event.target.value,
      });
      this.state.sources.push(event.target.value);
    }

    this.state.map.addLayer(mapLayers[event.target.value]);
    this.setState({ currentLayer: event.target.value + '_style' });
  };

  render() {
    return (
      <React.Fragment>
        {/* NOTE: sets mapbox and data-controls correctly */}
        <AutoSizer disableHeight={true} disableWidth={true}>
          {() => <div id='map' style={{ position: 'absolute', top: 0, bottom: 0, width: '100%', height: '100%' }} />}
        </AutoSizer>
        <div className='data-controls' style={styles.dataControls}>
          <select onChange={this.updateMap}>
            <option value='none' disabled selected>
              Choose A Dataset
            </option>
            <option value='none'>None</option>
            <option value='traffic_volume'>Traffic Congestion</option>
            <option value='road_works_and_event_lines'>Road Works</option>
            <option value='emergency_road_closure_lines'>Emergency Road Closures</option>
          </select>
        </div>
      </React.Fragment>
    );
  }
}

export default MapboxMap;
