import React from 'react';

/** DocumentTitle component sets the document title. */
class DocumentTitle extends React.Component {
  componentDidMount() {
    this.setTitle();
  }
  componentDidUpdate() {
    this.setTitle();
  }
  setTitle() {
    document.title = typeof this.props.children === 'string' ? this.props.children : '';
  }
  render() {
    return null;
  }
}

export default DocumentTitle;
