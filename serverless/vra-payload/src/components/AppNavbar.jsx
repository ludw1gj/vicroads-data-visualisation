import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TrafficIcon from '@material-ui/icons/Traffic';

import AppDrawer from './AppDrawer';
import { routes } from '../services/config';
import { windowLocationContains } from '../services/util';

const NavbarLink = styled(Link)`
  text-decoration: none;
  color: inherit;
`;

const NavbarLinkNonRouted = styled.a`
  text-decoration: none;
  color: inherit;
`;

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  barColour: {
    backgroundColor: 'rgb(80, 170, 75)',
  },
};

class AppNavbar extends React.Component {
  state = {
    drawerActive: false,
  };

  /** Toggle the drawer
   * @param {boolean} active - is the toggle active.
   */
  toggleDrawer = active => () => {
    this.setState({ drawerActive: active });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position='static' style={styles.barColour}>
          <Toolbar>
            <IconButton
              className={classes.menuButton}
              color='inherit'
              aria-label='Menu'
              onClick={this.toggleDrawer(true)}>
              <MenuIcon />
            </IconButton>
            <TrafficIcon style={{ paddingRight: '5px', paddingBottom: '4px' }} />
            <Typography variant='h6' color='inherit' className={classes.grow}>
              VicRoads Analytics
            </Typography>
            {routes.map(route => {
              if (windowLocationContains('/map')) {
                return (
                  <NavbarLinkNonRouted href={route.url} key={route.name + '-link'}>
                    <Button color='inherit'>{route.name}</Button>
                  </NavbarLinkNonRouted>
                );
              }
              return (
                <NavbarLink to={route.url} key={route.name + '-link'}>
                  <Button color='inherit'>{route.name}</Button>
                </NavbarLink>
              );
            })}
          </Toolbar>
        </AppBar>
        <AppDrawer active={this.state.drawerActive} toggler={this.toggleDrawer} />
      </div>
    );
  }
}

export default withStyles(styles)(AppNavbar);
