import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import KeplerGl from 'kepler.gl';
import { forwardTo } from 'kepler.gl/actions';
// @ts-ignore
import { addDataToMap, removeDataset } from 'kepler.gl/actions';
import { AutoSizer } from 'react-virtualized';

import { makeReadonly, defaultState } from '../store/actions/keplerActions';
import { getGeoJSONDataset } from '../store/actions/keplerDataActions';
import { queryGeoJSONDataset } from '../services/api';
import { MAPS_PAST_DATA_API_URL, KEPLER_MAP_ID } from '../services/config';

import keplerStateTrafficData from '../assets/config/keplergl/keplerStateTrafficData.json';
import keplerStateCrashData from '../assets/config/keplergl/keplerStateCrashData.json';

const NONE_SELECTED = 'none';
const TRAFFIC_DATASET_ID = 'traffic_volume';
const CRASH_DATASET_ID = 'crashes_last_five_years';

const MapContainer = styled.div`
  display: flex;
  position: absolute;

  height: calc(100vh - 64px);
  width: 100%;
`;

const TOKEN = 'pk.eyJ1IjoibHVkdzFnaiIsImEiOiJjanJiZnhoa3ExMm41M3lzNzFseGd3bjhkIn0.j9K72tF4bEidtjQxEw6C8Q';

const styles = {
  dataControls: {
    position: 'absolute',
    zIndex: 1,
    top: '10px',
    left: '10px',
  },
};

class KeplerMap extends React.Component {
  componentDidMount() {
    this.props.keplerGlDispatch(makeReadonly());
    this.props.keplerGlDispatch(defaultState());
  }

  removePreviousDataset = datasetID => {
    switch (datasetID) {
      case TRAFFIC_DATASET_ID:
        this.props.keplerGlDispatch(removeDataset(CRASH_DATASET_ID));
      case CRASH_DATASET_ID:
        this.props.keplerGlDispatch(removeDataset(TRAFFIC_DATASET_ID));
      case NONE_SELECTED:
        this.props.keplerGlDispatch(removeDataset(TRAFFIC_DATASET_ID));
        this.props.keplerGlDispatch(removeDataset(CRASH_DATASET_ID));
    }
  };

  updateMap = event => {
    event.preventDefault();

    const datasetID = event.target.value;
    this.removePreviousDataset(datasetID);
    if (datasetID === NONE_SELECTED) {
      return;
    }

    const url = `${MAPS_PAST_DATA_API_URL}${datasetID}`;
    queryGeoJSONDataset(url, event.target.value).then(dataset => {
      const dispatch = config => this.props.keplerGlDispatch(addDataToMap({ datasets: dataset, config: config }));
      if (datasetID === TRAFFIC_DATASET_ID) {
        dispatch(keplerStateTrafficData);
      }
      if (datasetID === CRASH_DATASET_ID) {
        dispatch(keplerStateCrashData);
      }
    });
  };

  render() {
    return (
      <MapContainer>
        <div className='data-controls' style={styles.dataControls}>
          <select onChange={this.updateMap}>
            <option value='none' disabled selected>
              Choose A Dataset
            </option>
            <option value={NONE_SELECTED}>None</option>
            <option value={CRASH_DATASET_ID}>Crash Data</option>
            <option value={TRAFFIC_DATASET_ID}>Traffic Data</option>
          </select>
        </div>
        <AutoSizer>
          {({ height, width }) => (
            <KeplerGl id={this.props.id} height={height} width={width} mapboxApiAccessToken={TOKEN} />
          )}
        </AutoSizer>
      </MapContainer>
    );
  }
}

const mapStateToProps = state => ({ keplerGl: state.keplerGl, keplerData: state.keplerData });

const mapDispatchToProps = (dispatch, props) => ({
  dispatch,
  keplerGlDispatch: forwardTo(KEPLER_MAP_ID, dispatch),
  getGeoJSONDataset: (url, id) => dispatch(getGeoJSONDataset(url, id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KeplerMap);
