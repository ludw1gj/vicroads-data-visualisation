import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';

import { routes } from '../services/config';
import { windowLocationContains } from '../services/util';

const DrawerLink = styled(Link)`
  text-decoration: none;
  color: inherit;
`;

const DrawerLinkNonRouted = styled.a`
  text-decoration: none;
  color: inherit;
`;

const styles = {
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
};

const AppDrawer = ({ active, toggler, classes }) => {
  const createListItem = route => {
    return (
      <ListItem button key={route.name}>
        <ListItemIcon>
          <route.icon />
        </ListItemIcon>
        <ListItemText primary={route.name} />
      </ListItem>
    );
  };

  const sideList = (
    <div className={classes.list}>
      <List>
        {routes.map(route => {
          if (windowLocationContains('/map')) {
            return (
              <DrawerLinkNonRouted href={route.url} key={route.name + '-link'}>
                {createListItem(route)}
              </DrawerLinkNonRouted>
            );
          }
          return (
            <DrawerLink to={route.url} key={route.name + '-link'}>
              {createListItem(route)}
            </DrawerLink>
          );
        })}
      </List>
    </div>
  );

  return (
    <Drawer open={active} onClose={toggler(false)}>
      <div tabIndex={0} role='button' onClick={toggler(false)} onKeyDown={toggler(false)}>
        {sideList}
      </div>
    </Drawer>
  );
};

export default withStyles(styles)(AppDrawer);
