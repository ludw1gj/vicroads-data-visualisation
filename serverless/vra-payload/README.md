# VicRoads Analytics Client Payload

This is the frontend client code written using React, Redux, and Material UI.

## Development

In the project directory, you can run:

#### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

#### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

#### `yarn run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Dependencies

#### run `yarn install` to install dependencies

## Production

#### How to deploy

Run `serverless deploy` (this command may take a long while - 5min? - as Cloudfront takes a lengthy
time to deploy), also `yarn run build` is automatically run.

#### How to un-deploy

Run `serverless remove`, this will delete the deployed stack.

## Notes

- When executing commands make sure you `cd` into this directory.
- bucketName in serverless.yml must be unique. If a bucket of the same name already exists, the
  `serverless deploy` will fail unless the local bucketName is changed.

### Other

- Image from: https://pixabay.com/en/road-highway-desert-landscape-3489206/
