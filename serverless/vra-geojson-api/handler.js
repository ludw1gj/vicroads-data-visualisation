/*******************************************************************************
 * vra-dynamo-api/handler.js
 * 
 * Author:        Stephen Radley
 * Creation Date: 2019/01/27
 * 
 * ...
 ******************************************************************************/

'use strict';

const axios = require('axios');
const AWS = require('aws-sdk');
const constants = require('./constants');
const helper = require('./helper');

const s3 = new AWS.S3();

/**
 * @function pastData ...
 * 
 * GET: api/maps/pastData/?type=traffic_volume
 *                              traffic_count_locations
 *                              crashes_last_five_years
 *                              road_works_and_event_lines
 *                              emergency_road_closure_lines
 */
module.exports.pastData = (event, context, callback) => {
  // no type specified
  if (!event.pathParameters) {
    const resp = {
      statusCode: 400,
      body: JSON.stringify({
        message: 'No type specified.'
      }),
      headers: {}
    };
    callback(null, resp);
    return;
  }
  if (!event.pathParameters.type) {
    const resp = {
      statusCode: 400,
      body: JSON.stringify({
        message: 'No type specified.'
      }),
      headers: {}
    };
    callback(null, resp);
    return;
  }

  var type = event.pathParameters.type;

  // invalid type specified
  if (!Object.keys(constants.PAST_DATA).includes(type)) {
    const resp = {
      statusCode: 400,
      body: JSON.stringify({
        message: 'Invalid type specified.'
      }),
      headers: {}
    };
    callback(null, resp);
    return;
  }

  const resp = {
    statusCode: 301,
    headers: {
      Location: `https://s3-ap-southeast-2.amazonaws.com/${process.env.BUCKET}/${constants.PAST_DATA[type]}.json`,
      ContentType: 'application/json'
    },
    body: ''
  };
  callback(null, resp);
};

/**
 * @function realTime ...
 * 
 * GET: /maps/realTime/?type=traffic_volume
 *                           road_works_and_event_lines
 *                           emergency_road_closure_lines
 */
module.exports.realTime = (event, context, callback) => {
  // no type specified
  if (!event.pathParameters) {
    const resp = {
      statusCode: 400,
      body: JSON.stringify({
        message: 'No type specified.'
      }),
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      }
    };
    callback(null, resp);
    return;
  }
  if (!event.pathParameters.type) {
    const resp = {
      statusCode: 400,
      body: JSON.stringify({
        message: 'No type specified.'
      }),
      headers: {}
    };
    callback(null, resp);
    return;
  }

  var type = event.pathParameters.type;

  // invalid type specified
  if (!Object.keys(constants.REAL_TIME).includes(type)) {
    const resp = {
      statusCode: 400,
      body: JSON.stringify({
        message: 'Invalid type specified.'
      }),
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true}
    };
    callback(null, resp);
    return;
  }

  var params = {
    Key: constants.REAL_TIME[type] + '.json',
    Bucket: process.env.BUCKET
  };
  s3.headObject(params, (err, metadata) => {
    if (err && (err.code == 'Forbidden' || err.code == 'NotFound')) {
      helper.uploadURLToS3(constants.VR_URL+'&TYPENAMES=vicroads:'+constants.REAL_TIME[type]+'&AUTH='+process.env.VR_API_TOKEN, constants.REAL_TIME[type]+'.json', (res, err) => {
        if (err) {
          console.error(err);
        } else {
          console.log(`Successfully downloaded file: ${constants.REAL_TIME[type].json}`);
          var resp = {
            statusCode: 301,
            headers: {
              Location: `https://s3-ap-southeast-2.amazonaws.com/${process.env.BUCKET}/${constants.REAL_TIME[type]}.json`,
              ContentType: 'application/json',
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Credentials': true
            },
            body: ''
          };
          callback(null, resp);
          return;
        }
      });
    } else {
      var secsBetween = (Date.now() - Date.parse(metadata.LastModified)) / 1000;
      if (secsBetween > 900) { // we download a new one
        helper.uploadURLToS3(constants.VR_URL+'&TYPENAMES=vicroads:'+constants.REAL_TIME[type]+'&AUTH='+process.env.VR_API_TOKEN, constants.REAL_TIME[type]+'.json', (res, err) => {
          if (err) {
            console.error(err);
          } else {
            console.log(`Successfully downloaded file: ${constants.REAL_TIME[type].json}`);
            
            var resp = {
              statusCode: 301,
              headers: {
                Location: `https://s3-ap-southeast-2.amazonaws.com/${process.env.BUCKET}/${constants.REAL_TIME[type]}.json`,
                ContentType: 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
              },
              body: ''
            };
            callback(null, resp);
            return;
          }
        });
      } else {
        // we return the current one
        var resp = {
          statusCode: 301,
          headers: {
            Location: `https://s3-ap-southeast-2.amazonaws.com/${process.env.BUCKET}/${constants.REAL_TIME[type]}.json`,
            ContentType: 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
          },
          body: ''
        };
        callback(null, resp);
        return;
      }
    }
  });
};