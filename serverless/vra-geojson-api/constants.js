/*******************************************************************************
 * vra-dynamo-api/constants.js
 * 
 * Author:        Stephen Radley
 * Creation Date: 2019/01/27
 * 
 * ...
 ******************************************************************************/

/** @constant {Object} CONSTANTS ... */
const CONSTANTS = {
  PAST_DATA: {
    traffic_volume: '147696bb47544a209e0a5e79e165d1b0_0',
    traffic_count_locations: 'a2b2011fab9a42d38dc5ea07420a9823_0',
    crashes_last_five_years: 'c2a69622ebad42e7baaa8167daa72127_0',
    road_works_and_event_lines: '43ed105071544895a30090fb5261f1a6_0',
    emergency_road_closure_lines: '590a168386644b128c3fbb70628602cd_0'
  },
  REAL_TIME: {
    traffic_volume: 'bluetooth_links',
    road_works_and_event_lines: 'rwe_line',
    emergency_road_closure_lines: 'erc_line'
  },
  VR_URL: `http://api.vicroads.vic.gov.au/vicroads/wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&OUTPUTFORMAT=application/json`
  // + `&AUTH={process.env.VIC_ROADS_TOKEN}&TYPENAMES={constants.REAL_TIME.???}`
};

module.exports = Object.freeze(CONSTANTS);  // freeze prevents changes by users
