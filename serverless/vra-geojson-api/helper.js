/*******************************************************************************
 * vra-geojson-api/helper.js
 * 
 * Author:        Stephen Radley
 * Creation Date: 2019/01/21
 * 
 * ...
 ******************************************************************************/

const axios = require('axios');
const AWS = require('aws-sdk');

const s3 = new AWS.S3();

/**
 * @function uploadURLToS3 downloads a file and stores it in the S3 bucket
 * specified as an environment variable.
 */
module.exports.uploadURLToS3 = (location, filename, callback) => {
  var params = {
    url: location,
    method: 'GET',
    responseType: 'arraybuffer'
  };

  // download file into memory
  axios(params)
    .then(res => {
      // if not OK, throw error
      if (res.status !== 200)
        throw new Error(`Failed to download file ${filename}: ${res.status}: ${res.statusText}`);
      // else continue
      return res;
    })
    .then(res => {
      // store in S3 bucket
      return s3.putObject({
        Bucket: process.env.BUCKET,
        Key: filename,
        Body: res.data,
        ACL: 'public-read',
        ContentType: 'application/json'
      }).promise();
    })
    .then(res => {
      callback(res, null)  // success
    })
    .catch(err => {
      callback(null, err)  // failure
    });
};