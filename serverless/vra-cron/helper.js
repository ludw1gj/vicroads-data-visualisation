/*******************************************************************************
 * vra-cron/helper.js
 * 
 * Author:        Stephen Radley
 * Creation Date: 2019/01/21
 * 
 * ...
 ******************************************************************************/

const axios = require('axios');
const AWS = require('aws-sdk');
const { Pool } = require('pg');

const constants = require('./constants');

const s3 = new AWS.S3();
const pool = new Pool();

/**
 * @function uploadURLToS3 downloads a file and stores it in the S3 bucket
 * specified as an environment variable.
 */
module.exports.uploadURLToS3 = (location, filename, callback) => {
  var params = {
    url: location,
    method: 'GET',
    responseType: 'arraybuffer'
  };

  // download file into memory
  axios(params)
    .then(res => {
      // if not OK, throw error
      if (res.status !== 200)
        throw new Error(`Failed to download file ${filename}: ${res.status}: ${res.statusText}`);
      // else continue
      return res;
    })
    .then(res => {
      var s3params = {
        Bucket: process.env.BUCKET,
        Key: filename,
        Body: res.data,
        ACL: 'private'
      };

      if (filename.includes('.json')) {
        s3params.ACL = 'public-read';
        s3params.ContentType = 'application/json';
      }
      // store in S3 bucket
      return s3.putObject(s3params).promise();
    })
    .then(res => {
      callback(res, null)  // success
    })
    .catch(err => {
      callback(null, err)  // failure
    });
};

/**
 * @function updateRedshiftTable ...
 */
module.exports.updateRedshiftTable = (filecode, callback) => {
  // construct copy query
  var copy = `COPY ${constants.TABLE_DATA[filecode].name} FROM 's3://${process.env.BUCKET}/${filecode}.csv'
CREDENTIALS 'aws_access_key_id=${process.env.ACCESS_ID};aws_secret_access_key=${process.env.SECRET_KEY}'
CSV IGNOREHEADER 1;`;

  // construct first query
  var query = `DROP TABLE ${constants.TABLE_DATA[filecode].name};`;
  //var query = constants.TABLE_DATA[filecode].create;

  // drop table specified by constants.TABLE_DATA[filecode].name
  pool.query(query)
    .then(() => {
      // create table using command stored at constants.TABLE_DATA[filecode].create
      query = constants.TABLE_DATA[filecode].create;
      return pool.query(query);
    })
    .then(() => {
      // copy data from file into table
      return pool.query(copy);
    })
    .then(res => {
      callback(res, null);  // success
    })
    .catch(err => {
      callback(null, err);  // failure
    });
};