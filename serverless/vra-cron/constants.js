/*******************************************************************************
 * vra-cron/constants.js
 * 
 * Author:        Stephen Radley
 * Creation Date: 2019/01/21
 * 
 * ...
 ******************************************************************************/

/** @constant {Object} CONSTANTS ... */
const CONSTANTS = {
  // vicroads download url
  VR_URL: 'https://opendata.arcgis.com/datasets/',
  // file name codes
  FILES: [
    '147696bb47544a209e0a5e79e165d1b0_0',  // Traffic Volume
    'a2b2011fab9a42d38dc5ea07420a9823_0',  // Traffic Count Locations
    'c2a69622ebad42e7baaa8167daa72127_0',  // Crashes Last Five Years
    '43ed105071544895a30090fb5261f1a6_0',  // Road Works And Event Lines
    '590a168386644b128c3fbb70628602cd_0'   // Emergency Road Closure Lines
  ],
  // table name and SQL creation query
  TABLE_DATA: {
    '147696bb47544a209e0a5e79e165d1b0_0': {  // Traffic Volume
      name: 'traffic_volume',
      create: `CREATE TABLE traffic_volume (
  OBJECTID			  INT NOT NULL,
  LOCATION_ID		  INT NOT NULL,
  MIDPNT_LAT		  TEXT NOT NULL,
  MIDPNT_LON		  TEXT NOT NULL,
  HMGNS_FLOW_ID		INT NOT NULL,
  HMGNS_LNK_ID		INT NOT NULL,
  HMGNS_LNK_DESC	TEXT NOT NULL,
  FLOW				    TEXT NOT NULL,
  ALLVEHS_MMW		  INT NOT NULL,
  ALLVEH_CALC		  VARCHAR(1) NOT NULL,
  ALLVEHS_AADT		INT NOT NULL,
  TRUCKS_AADT		  INT NOT NULL,
  TRUCK_CALC		  VARCHAR(1) NOT NULL,
  PER_TRUCKS		  FLOAT,
  TWO_WAY_AADT		INT NOT NULL,
  ALLVEH_AMPEAK		INT,
  ALLVEH_PMPEAK		INT,
  GROWTH_RATE		  TEXT NOT NULL,
  CI				      TEXT NOT NULL,
  YR				      INT,
  LABEL				    TEXT NOT NULL
);`
    },
    'a2b2011fab9a42d38dc5ea07420a9823_0': {  // Traffic Count Locations
      name: 'traffic_count_locations',
      create: `CREATE TABLE traffic_count_locations (
  FID         TEXT,
  OBJECTID    INT NOT NULL,
  TFM_ID      INT NOT NULL,
  TFM_DESC    TEXT NOT NULL,
  TFM_TYP_DE  TEXT NOT NULL,
  MOVEMENT_T  TEXT NOT NULL,
  SITE_DESC   TEXT NOT NULL,
  ROAD_NBR    INT NOT NULL,
  DECLARED_R  TEXT NOT NULL,
  LOCAL_ROAD  TEXT NOT NULL,
  DATA_SRC_C  TEXT NOT NULL,
  DATA_SOURC  TEXT NOT NULL,
  TIME_CATEG  TEXT NOT NULL,
  YEAR_SINCE  INT NOT NULL,
  LAST_YEAR   INT NOT NULL,
  AADT_ALLVE  INT NOT NULL,
  AADT_TRUCK  INT NOT NULL,
  PER_TRUCKS  FLOAT NOT NULL
);`
    },
    'c2a69622ebad42e7baaa8167daa72127_0': {  // Crashes Last Five Years
      name: 'crashes_last_five_years',
      create: `CREATE TABLE crashes_last_five_years (
  X                 TEXT NOT NULL,
  Y                 TEXT NOT NULL,
  OBJECTID          INT NOT NULL,
  ACCIDENT_NO       TEXT NOT NULL,
  ABS_CODE          TEXT NOT NULL,
  ACCIDENT_STATUS   TEXT NOT NULL,
  ACCIDENT_DATE     TEXT NOT NULL,
  ACCIDENT_TIME     TEXT NOT NULL,
  ALCOHOLTIME       TEXT NOT NULL,
  ACCIDENT_TYPE     TEXT NOT NULL,
  DAY_OF_WEEK       TEXT NOT NULL,
  DCA_CODE          TEXT NOT NULL,
  HIT_RUN_FLAG      TEXT NOT NULL,
  LIGHT_CONDITION   TEXT NOT NULL,
  POLICE_ATTEND     TEXT NOT NULL,
  ROAD_GEOMETRY     TEXT NOT NULL,
  SEVERITY          TEXT NOT NULL,
  SPEED_ZONE        TEXT NOT NULL,
  RUN_OFFROAD       TEXT NOT NULL,
  NODE_ID           TEXT NOT NULL,
  LONGITUDE         TEXT NOT NULL,
  LATITUDE          TEXT NOT NULL,
  NODE_TYPE         TEXT NOT NULL,
  LGA_NAME          TEXT NOT NULL,
  REGION_NAME       TEXT NOT NULL,
  VICGRID_X         TEXT NOT NULL,
  VICGRID_Y         TEXT NOT NULL,
  TOTAL_PERSONS     INT NOT NULL,
  INJ_OR_FATAL      INT NOT NULL,
  FATALITY          INT NOT NULL,
  SERIOUSINJURY     INT NOT NULL,
  OTHERINJURY       INT NOT NULL,
  NONINJURED        INT NOT NULL,
  MALES             INT NOT NULL,
  FEMALES           INT NOT NULL,
  BICYCLIST         INT NOT NULL,
  PASSENGER         INT NOT NULL,
  DRIVER            INT NOT NULL,
  PEDESTRIAN        INT NOT NULL,
  PILLION           INT NOT NULL,
  MOTORIST          INT NOT NULL,
  UNKNOWN           INT NOT NULL,
  PED_CYCLIST_5_12  INT NOT NULL,
  PED_CYCLIST_13_18 INT NOT NULL,
  OLD_PEDESTRIAN    INT NOT NULL,
  OLD_DRIVER        INT NOT NULL,
  YOUNG_DRIVER      INT NOT NULL,
  ALCOHOL_RELATED   TEXT NOT NULL,
  UNLICENSED        INT NOT NULL,
  NO_OF_VEHICLES    INT NOT NULL,
  HEAVYVEHICLE      INT NOT NULL,
  PASSENGERVEHICLE  INT NOT NULL,
  MOTORCYCLE        INT NOT NULL,
  PUBLICVEHICLE     INT NOT NULL,
  DEG_URBAN_NAME    TEXT NOT NULL,
  DEG_URBAN_ALL     TEXT NOT NULL,
  LGA_NAME_ALL      TEXT NOT NULL,
  REGION_NAME_ALL   TEXT NOT NULL,
  SRNS              TEXT,
  SRNS_ALL          TEXT,
  RMA               TEXT,
  RMA_ALL           TEXT NOT NULL,
  DIVIDED           TEXT NOT NULL,
  DIVIDED_ALL       TEXT NOT NULL,
  STAT_DIV_NAME     TEXT NOT NULL
);`
    },
    '43ed105071544895a30090fb5261f1a6_0': {  // Road Works and Event Lines
      name: 'road_works_and_event_lines',
      create: `CREATE TABLE road_works_and_event_lines (
  OBJECTID              INT NOT NULL,
  LOCATION_ID           INT NOT NULL,
  RWE_TYPE              TEXT NOT NULL,
  RWE_CLOSURE_TYPE      TEXT NOT NULL,
  RWE_STATUS            TEXT NOT NULL,
  RWE_START_DATE        TEXT NOT NULL,
  RWE_END_DATE          TEXT NOT NULL,
  RWE_PUBLISH_AFTER     TEXT NOT NULL,
  RWE_PUBLISH_TEXT      TEXT NOT NULL,
  LRS_ROADSEARCH        TEXT NOT NULL,
  SUBJECT_PREF_ROADNAME TEXT NOT NULL,
  LRS_DEC_RDNAME        TEXT,
  LRS_START_INT_DIST    INT NOT NULL,
  LRS_START_INT_DIR     TEXT,
  START_PREF_RDNAME     TEXT NOT NULL,
  LRS_START_LOCALITY    TEXT,
  LRS_END_INT_DIST      INT NOT NULL,
  LRS_END_INT_DIR       TEXT,
  END_PREF_RDNAME       TEXT,
  LRS_END_LOCALITY      TEXT,
  MIDPNT_LAT            TEXT NOT NULL,
  MIDPNT_LON            TEXT NOT NULL,
  LRS_RMACLASS          TEXT NOT NULL,
  LRS_SRNS              TEXT,
  LRS_LGA               TEXT NOT NULL,
  LRS_VR_REG            TEXT NOT NULL,
  LRS_SES_REGION        TEXT NOT NULL,
  LRS_MELWAY            TEXT,
  LRS_VCSD              TEXT NOT NULL,
  LRS_TRAM_ROUTE        TEXT,
  LRS_BUS_ROUTE         TEXT,
  DT_CREATED            TEXT NOT NULL,
  DT_MODIFIED           TEXT NOT NULL,
  RWE_DT_LN_UPDATE      TEXT NOT NULL
);`
    },
    '590a168386644b128c3fbb70628602cd_0': {  // Emergency Road Closure Lines
      name: 'emergency_road_closure_lines',
      create: `CREATE TABLE emergency_road_closure_lines (
  OBJECTID                INT NOT NULL,
  ERC_ID                  TEXT NOT NULL,
  OBJECTID_1              INT NOT NULL,
  ERC_ID_1                TEXT NOT NULL,
  INCIDENT_TYPE           TEXT NOT NULL,
  INCIDENT_STATUS         TEXT NOT NULL,
  COMMS_COMMENT           TEXT,
  ROAD_CLOSURE_TYPE       TEXT NOT NULL,
  DECLARED_ROAD_NUMBER    INT,
  DECLARED_ROAD_NAME      TEXT,
  DECLARED_ROAD_DIRECTION TEXT NOT NULL,
  CLOSED_ROAD_NAME        TEXT NOT NULL,
  CLOSED_ROAD_SRNS        TEXT,
  CLOSED_ROAD_RMA_CLASS   TEXT NOT NULL,
  CLOSED_ROAD_LGA         TEXT NOT NULL,
  CLOSED_ROAD_VR_REGION   TEXT NOT NULL,
  CLOSED_ROAD_SES_REGION  TEXT NOT NULL,
  CLOSED_ROAD_TRAM        TEXT,
  CLOSED_ROAD_BUS         TEXT,
  INCIDENT_DISTANCE       INT NOT NULL,
  INCIDENT_DIRECTION      TEXT,
  INCIDENT_ROAD_NAME      TEXT,
  INCIDENT_LOCALITY       TEXT NOT NULL,
  INCIDENT_MELWAY         TEXT,
  INCIDENT_VCSD           TEXT NOT NULL,
  START_INT_ROAD_NAME     TEXT NOT NULL,
  START_INT_LOCALITY      TEXT NOT NULL,
  END_INT_ROAD_NAME       TEXT NOT NULL,
  END_INT_LOCALITY        TEXT NOT NULL,
  DT_CREATED              TEXT NOT NULL,
  DT_UPDATED              TEXT NOT NULL,
  DT_PUBLISH_UNTIL        TEXT,
  DT_LAST_LN_UPDATE       TEXT NOT NULL,
  POINT_TYPE              TEXT NOT NULL
);`
    }
  }
};

module.exports = Object.freeze(CONSTANTS);  // freeze prevents changes by users