# VicRoads Analytics Cron Lambdas

Lambdas regarding the maintenance of the VicRoads data, keeping the .csv and .geojson files up to date in our S3 bucket used for DynamoDB and Redshift, also updating the databases.