/*******************************************************************************
 * vra-cron/handler.js
 * 
 * Author:        Stephen Radley
 * Creation Date: 2019/01/21
 * 
 * ...
 ******************************************************************************/

'use strict';

const helper = require('./helper');
const constants = require('./constants');

/**
 * @function updateDatabase ...
 */
module.exports.updateDatabase = () => {
  for (var i = 0; i < constants.FILES.length; i++) {

    var filecode = `${constants.FILES[i]}`;
    var url = `${constants.VR_URL}${filecode}`;

    // csv file
    if (i < 3) {
      helper.uploadURLToS3(url+'.csv', filecode+'.csv', (res, err) => {
        if (err) {
          console.error(err);
        } else {
          console.log(`Successfully downloaded file: ${filecode}.csv`);
          // update redshift table
          helper.updateRedshiftTable(filecode, (res, err) => {
            if (err) {
              console.error(err);  // upon failure
            } else {
              console.log(`Successfully updated Redshift table ${constants.TABLE_DATA[filecode].name}`);
            }
          });
        }
      });
    }

    // geojson file
    helper.uploadURLToS3(url+'.geojson', filecode+'.json', (res, err) => {
      if (err) {
        console.error(err);
      } else {
        console.log(`Successfully downloaded file: ${filecode}.json`);
      }
    });
  }
};